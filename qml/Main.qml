import Felgo 3.0 // Felgo Module
import QtQuick 2.0 // QtQuick Module

App {

  NavigationStack {

    Page {
      id: page
      title: "Sign in"
        MouseArea {
          anchors.fill: parent
          onClicked: userEdit.focus = false
        }

        // background for input
        Rectangle {
          anchors.fill: userEdit
          anchors.margins: -dp(8)
          color: "lightgrey"
        }

        // input
        AppTextInput {
          id: userEdit
          width: dp(200)
          placeholderText: "What's your name?"
          anchors.centerIn: parent
          property bool isTooShort: userEdit.text.length < 1
        }

        AppText {
          text: "Please enter a valid name."
          color: "red"
          anchors.top: userEdit.bottom
          anchors.topMargin: dp(16)
          anchors.left: userEdit.left
          visible: userEdit.isTooShort
        }

      AppButton {
        anchors.centerIn: top
        text: "Sign in"
        onClicked: {
          // You need to specify the NavigationStack where to push the new page
          // You can either give the NavigationStack and id, or like in this example, use the navigationStack property of the current page
          page.navigationStack.push(signInPage)
        }
      }




    }
  }

  Component {
    id: signInPage
    Page {
      title: "Hi " + userEdit.text
      MouseArea {
        anchors.fill: parent
        onClicked: searchEdit.focus = false
      }

      // input
      AppTextField
      {
        id: searchEdit
        width: dp(200)
        placeholderText: "Search Chats"
        anchors.centerIn: left
      }

      }
    }
  }
